import { useEffect } from 'react'
import { useGlobal } from '../Components'

function usePagination() {
	const { state, currentPage, profilePerPage, setCurrentPage } = useGlobal()
	const lastPostIndex = currentPage * profilePerPage
	const firstPostIndex = lastPostIndex - profilePerPage
	const pagePosts = state.filtered?.slice(firstPostIndex, lastPostIndex)
	useEffect(() => {
		if (pagePosts?.length == 0 && currentPage != 1) {
			setCurrentPage(currentPage - 1)
		}
	}, [pagePosts, state])
	return [pagePosts]
}

export default usePagination
