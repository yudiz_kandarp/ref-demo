import { useEffect, useState } from 'react'

function useFetchApi(api) {
	const [data, setData] = useState()
	const [error, setError] = useState()
	const [isLoading, setIsLoadning] = useState(true)

	useEffect(function fetchingDataFromApi() {
		FetchApi()
	}, [])

	async function FetchApi() {
		try {
			setIsLoadning(true)
			const response = await fetch(api)
			const result = await response.json()
			setData(result)
			setIsLoadning(false)
		} catch (error) {
			setError(error)
			setIsLoadning(false)
		}
	}
	return [data, error, isLoading]
}

export default useFetchApi
