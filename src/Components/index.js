import Button from './Button/Button'
import Forms from './Forms/Forms'
import Search from './Search/Search'
import Pagination from './Pagination/Pagination'
import Tables from './Tables/Tables'
import Modal from './Modal/Modal'
import Header from './Header/Header'
import AddData from './AddData/AddData'
import UpdateData from './UpdateData/UpdateData'

export {
	Button,
	Forms,
	Search,
	Pagination,
	Tables,
	Modal,
	Header,
	AddData,
	UpdateData,
}

/* importing context for Components */
import { useGlobal } from '../Contexts/GlobalContext'
export { useGlobal }

/* importing Hooks */
import useFetchApi from '../Hooks/useFetchApi'
export { useFetchApi }

/* importing all css files here */
import '../Styles/button.scss'
import '../Styles/forms.scss'
import '../Styles/pagination.scss'
import '../Styles/search.scss'
import '../Styles/tables.scss'
import '../Styles/modal.scss'
import '../Styles/header.scss'
