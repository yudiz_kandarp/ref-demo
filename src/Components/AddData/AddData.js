import React from 'react'
import { Modal, Forms, useGlobal } from '../index'

function AddData() {
	const { show, setShow } = useGlobal()
	return (
		<Modal show={show} onClose={() => setShow(false)}>
			<div className='form_container'>
				<Forms actions='submit' />
			</div>
		</Modal>
	)
}

export default AddData
