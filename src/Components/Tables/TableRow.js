import React from 'react'
import { useGlobal } from '../index'
import PropTypes from 'prop-types'
import { Button } from '../index'

function TableRow({
	person: {
		login: { uuid },
		name,
		dob,
		phone,
	},
}) {
	const { state, dispatch } = useGlobal()

	function updateData() {
		const index = state.filtered.findIndex((item) => item.login.uuid == uuid)
		const data = {
			name: { first: name },
			dob: { age: dob.age },
			phone: phone,
			index,
		}
		dispatch({
			type: 'EDIT_DATA_TEMP',
			payload: data,
		})
	}

	return (
		<>
			<tr key={uuid}>
				<td>{name.first}</td>
				<td>{phone}</td>
				<td>{dob.age}</td>
				<td className='table_button_container'>
					<Button onClick={updateData}>Edit</Button>
					<Button
						addClass='delete'
						onClick={() =>
							dispatch({
								type: 'DELETE_DATA',
								payload: uuid,
							})
						}
					>
						Delete
					</Button>
				</td>
			</tr>
		</>
	)
}
TableRow.propTypes = {
	person: PropTypes.shape({
		login: PropTypes.shape({
			uuid: PropTypes.string,
		}),
		name: PropTypes.shape({
			first: PropTypes.string,
		}),
		dob: PropTypes.object,
		phone: PropTypes.string,
	}),
}
export default TableRow
