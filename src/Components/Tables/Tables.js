import React, { useEffect } from 'react'
import { useFetchApi, useGlobal } from '../index'
import usePagination from '../../Hooks/usePagination'
import TableRow from './TableRow'
import { Button } from '../index'

function Tables() {
	const { dispatch, setShow } = useGlobal()
	const [pagePosts] = usePagination()
	const api = 'https://randomuser.me/api/?results=50'

	const [data, , isLoading] = useFetchApi(api)

	useEffect(() => {
		dispatch({ type: 'FETCH_DATA', payload: data?.results })
	}, [data])

	return (
		<>
			<div className='table_container'>
				{isLoading ? (
					<h3>Loading ...</h3>
				) : (
					<table>
						<tbody>
							<tr>
								<th>Name</th>
								<th>Phone Number</th>
								<th>Age</th>
								<th></th>
							</tr>
							{pagePosts?.map((person) => {
								return <TableRow key={person?.login?.uuid} person={person} />
							})}
						</tbody>
					</table>
				)}
			</div>

			<Button addClass='add_btn' onClick={() => setShow(true)}>
				+
			</Button>
		</>
	)
}

export default Tables
