import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { Button } from '../index'
function Modal({ children, show, onClose }) {
	const modalRef = useRef(null)
	useEffect(() => {
		if (show) {
			modalRef.current.classList.add('visible')
		} else {
			modalRef.current.classList.remove('visible')
		}
	}, [show])
	return (
		<React.Fragment>
			<div ref={modalRef} className='modal__wrap'>
				<Button onClick={onClose} addClass='close__btn'>
					X
				</Button>
				<div className='modal'>{children}</div>
			</div>
		</React.Fragment>
	)
}
Modal.propTypes = {
	children: PropTypes.any,
	show: PropTypes.bool,
	onClose: PropTypes.any,
}
export default Modal
