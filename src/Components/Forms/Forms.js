import React, { useEffect, useRef } from 'react'
import { Button } from '../index'
import PropTypes from 'prop-types'
import { useGlobal } from '../index'
import { v4 as uuidv4 } from 'uuid'

function Forms({ actions }) {
	const name = useRef(null)
	const phone = useRef(null)
	const age = useRef(null)
	const { state, dispatch, setShow } = useGlobal()

	function handleAddForm(e) {
		e.preventDefault()
		if (name.current?.value.trim() == '' || phone.current?.value.trim() == '') {
			return
		}
		dispatch({
			type: 'ADD_DATA',
			payload: {
				uuid: uuidv4(),
				name: name.current.value,
				phone: phone.current.value,
				age: Number(age.current.value),
			},
		})
		name.current.value = ''
		phone.current.value = ''
		age.current.value = ''
		setShow(false)
	}

	useEffect(() => {
		if (actions == 'save') {
			if (name.current) {
				name.current.value = state.editIndex?.name.first.first
				phone.current.value = state.editIndex?.phone
				age.current.value = Number(state.editIndex?.dob.age || 0)
			}
		}
	}, [state.editIndex])

	function handleEditForm(e) {
		e.preventDefault()
		if (name.current?.value.trim() == '' || phone.current?.value.trim() == '') {
			return
		}
		dispatch({
			type: 'EDIT_DATA',
			payload: {
				name: name.current.value,
				phone: phone.current.value,
				age: Number(age.current.value),
			},
		})
		dispatch({ type: 'CLOSE_EDIT_MODAL' })
		name.current.value = ''
		phone.current.value = ''
		age.current.value = ''
	}

	return (
		<>
			<form
				className='form_container'
				onSubmit={actions == 'submit' ? handleAddForm : handleEditForm}
			>
				<input
					className='input'
					placeholder='name'
					autoComplete='off'
					type='text'
					name='name'
					ref={name}
				/>
				<input
					className='input'
					placeholder='phone'
					type='text'
					autoComplete='off'
					name='phone'
					ref={phone}
				/>
				<input
					className='input'
					placeholder='age'
					type='number'
					name='age'
					ref={age}
				/>
				<Button addClass='form_btn'>{actions}</Button>
			</form>
		</>
	)
}
Forms.propTypes = {
	actions: PropTypes.string,
}
export default Forms
