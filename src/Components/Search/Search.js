/* eslint-disable react/prop-types */
import React, { forwardRef, useRef } from 'react'
import { useGlobal } from '../index'

function Search(props, ref) {
	const { state, dispatch } = useGlobal()
	const selectFilterBy = useRef(null)
	const SelectionRefAsc = useRef(null)
	const SelectionRefDec = useRef(null)

	function handleChange() {
		let newData
		if (selectFilterBy.current.value == 'age') {
			newData = state.filtered?.sort((a, b) => a.dob.age - b.dob.age)
			if (SelectionRefDec.current.checked) {
				newData = state.filtered?.sort((a, b) => b.dob.age - a.dob.age)
			}
		} else if (selectFilterBy.current.value == 'name') {
			newData = state.filtered?.sort((a, b) => {
				return a.name.first.toLowerCase() < b.name.first.toLowerCase()
					? -1
					: a.name.first.toLowerCase() > b.name.first.toLowerCase()
					? 1
					: 0
			})
			if (SelectionRefDec.current.checked) {
				newData = state.filtered?.sort((a, b) => {
					return a.name.first.toLowerCase() < b.name.first.toLowerCase()
						? 1
						: a.name.first.toLowerCase() > b.name.first.toLowerCase()
						? -1
						: 0
				})
			}
		} else {
			newData = state.filtered
		}
		dispatch({ type: 'FILTER_DATA', payload: newData })
	}

	// useImperativeHandle(
	// 	ref,
	// 	() => {
	// 		return {
	// 			allClear: () => console.log('hello'),
	// 		}
	// 	},
	// 	[ref]
	// )

	return (
		<>
			<form onSubmit={props.handleData}>
				<input
					className='input search'
					ref={ref}
					type='search'
					placeholder='search here'
				/>
			</form>
			<div className='filter'>
				<span>Filter by</span>
				<select
					ref={selectFilterBy}
					onChange={handleChange}
					className='button select'
					name='filter'
					defaultValue='name'
				>
					<option value='name'> name </option>
					<option value='age'> age </option>
				</select>
				<div className='radio '>
					<input
						onChange={handleChange}
						type='radio'
						name='asc'
						id='asc'
						defaultValue='asc'
						ref={SelectionRefAsc}
					/>
					<label htmlFor='asc'>Asc</label>
					<input
						onChange={handleChange}
						type='radio'
						name='asc'
						id='dec'
						defaultValue='dec'
						ref={SelectionRefDec}
					/>
					<label htmlFor='dec'>Dec</label>
				</div>
			</div>
		</>
	)
}
// Search.propTypes = {
// 	submitData: PropTypes.func,
// }
export default forwardRef(Search)
