import React from 'react'
import PropTypes from 'prop-types'

function Button({ primary, children, onClick, addClass }) {
	return (
		<button
			onClick={onClick}
			className={`button ${primary ? 'primary' : ''} ${addClass}`}
		>
			{children}
		</button>
	)
}
Button.propTypes = {
	children: PropTypes.any,
	onClick: PropTypes.func,
	primary: PropTypes.bool,
	addClass: PropTypes.string,
}

export default Button
