import React from 'react'
import { useGlobal } from '../../Contexts/GlobalContext'
import usePagination from '../../Hooks/usePagination'
import { Button } from '../index'
function Pagination() {
	const {
		TotalPosts,
		profilePerPage,
		setProfilePerPage,
		setCurrentPage,
		currentPage,
	} = useGlobal()
	const [pagePosts] = usePagination()

	const pages = []
	for (let i = 1; i <= Math.ceil(TotalPosts / profilePerPage); i++) {
		pages.push(i)
	}

	return (
		<>
			<div className='pagination_container'>
				<>
					{pagePosts?.length != 0 && (
						<div className='button_container'>
							{pages?.map((number) => {
								return (
									<Button
										key={number}
										onClick={() => setCurrentPage(number)}
										addClass={`pagination_btn ${
											currentPage == number ? 'active' : ''
										}`}
										primary
									>
										{number}
									</Button>
								)
							})}
						</div>
					)}
				</>

				<div className='set_pagination'>
					<span>persons per page:</span>
					<select
						className='button select'
						onChange={(e) => setProfilePerPage(Number(e.target.value))}
					>
						<option value='5'>5</option>
						<option value='10'>10</option>
						<option value='15'>15</option>
					</select>
				</div>
			</div>
		</>
	)
}

export default Pagination
