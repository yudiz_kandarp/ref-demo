import React, { useRef } from 'react'
import { Search, useGlobal } from '../index'

function Header() {
	const searchRef = useRef(null)
	const { state, dispatch } = useGlobal()

	function handleSearch(e) {
		e.preventDefault()
		if (searchRef.current) {
			const newData = state.persons?.filter((item) => {
				return item.name.first
					.toLowerCase()
					.includes(searchRef.current.value.toLowerCase())
			})
			dispatch({ type: 'FILTER_DATA', payload: newData })
			searchRef.current.value = ''
		}
	}
	return (
		<header>
			<Search handleData={handleSearch} ref={searchRef} />
		</header>
	)
}

export default Header
