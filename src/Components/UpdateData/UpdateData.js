import React from 'react'
import { Modal, Forms, useGlobal } from '../index'

function UpdateData() {
	const { state, dispatch } = useGlobal()
	return (
		<Modal
			show={state.showEditModal}
			onClose={() => dispatch({ type: 'CLOSE_EDIT_MODAL' })}
		>
			<div className='form_container'>
				<Forms actions='save' />
			</div>
		</Modal>
	)
}

export default UpdateData
