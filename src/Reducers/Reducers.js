export const initialState = {
	persons: [],
	filtered: [],
	showEditModal: false,
	editIndex: null,
}

export function globalReducer(state, action) {
	switch (action.type) {
		case 'FETCH_DATA':
			return {
				...state,
				persons: action.payload,
				filtered: action.payload,
			}

		case 'DELETE_DATA':
			return {
				...state,
				filtered: state.filtered.filter(function DeletePerson(person) {
					return action.payload !== person.login?.uuid
				}),
			}
		case 'ADD_DATA':
			return {
				...state,
				filtered: [
					...state.filtered,
					{
						login: { uuid: action.payload.uuid },
						name: { first: action.payload.name },
						phone: action.payload.phone,
						dob: { age: action.payload.age },
					},
				],
			}
		case 'EDIT_DATA':
			state.filtered[state.editIndex.index] = {
				...state.filtered[state.editIndex.index],
				name: { first: action.payload.name },
				dob: { age: action.payload.age },
				phone: action.payload.phone,
			}

			return {
				...state,
				filtered: state.filtered,
			}
		case 'EDIT_DATA_TEMP':
			return { ...state, showEditModal: true, editIndex: action.payload }

		case 'CLOSE_EDIT_MODAL':
			return { ...state, showEditModal: false }

		case 'FILTER_DATA':
			return {
				...state,
				filtered: action.payload,
			}

		default:
			return state
	}
}
