import React from 'react'
import { Header, Pagination, Tables, AddData, UpdateData } from './Components'

import GlobalContext from './Contexts/GlobalContext'

function App() {
	return (
		<GlobalContext>
			<Header />
			<Tables />
			<Pagination />
			<AddData />
			<UpdateData />
		</GlobalContext>
	)
}

export default App
