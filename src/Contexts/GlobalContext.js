import React, { createContext, useContext, useReducer, useState } from 'react'
import PropTypes from 'prop-types'
import { globalReducer, initialState } from '../Reducers'
const Global = createContext()

export function useGlobal() {
	return useContext(Global)
}

function GlobalContext({ children }) {
	const [state, dispatch] = useReducer(globalReducer, initialState)
	const [currentPage, setCurrentPage] = useState(1)
	const [profilePerPage, setProfilePerPage] = useState(5)
	const [show, setShow] = useState(false)
	return (
		<Global.Provider
			value={{
				state,
				dispatch,
				currentPage,
				setCurrentPage,
				profilePerPage,
				setProfilePerPage,
				show,
				setShow,
				TotalPosts: state.filtered?.length,
			}}
		>
			{children}
		</Global.Provider>
	)
}
GlobalContext.propTypes = {
	children: PropTypes.node,
}
export default GlobalContext
